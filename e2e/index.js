var reporter = require('multiple-cucumber-html-reporter');

var options = {
    theme: 'bootstrap',
    jsonDir: 'cypress/reports',
    reportPath: '../coverage',
    reportSuiteAsScenarios: true,
    scenarioTimestamp: true,
    launchReport: true,
};

reporter.generate(options);