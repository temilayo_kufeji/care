FROM strapi/base

WORKDIR ./

COPY ./app/package.json ./
COPY ./app/package-lock.json ./

RUN npm install

#COPY . .
COPY ./app ./

ENV NODE_ENV staging

RUN npm build

EXPOSE 5000

CMD ["npm", "start"]
