module.exports = {
    env: {
        commonjs: true,
        es6: true,
        node: true,
    },
    extends:'eslint:recommended',
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        "sourceType": "module",
        ecmaVersion: 2018,
    },
};